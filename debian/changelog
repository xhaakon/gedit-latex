gedit-latex-plugin (3.8.0-1) unstable; urgency=low

  [ Pietro Battiston ]
  * Imported Upstream version 3.8.0 (Closes: #721433, #726058)
  * Updated debian/control (Standards, gedit version, Vcs-* fields)

  [ Andrea Gasparini ]
  * Added Pietro as package Uploader

 -- Pietro Battiston <me@pietrobattiston.it>  Mon, 23 Sep 2013 14:27:50 +0200

gedit-latex-plugin (3.4.1-1) unstable; urgency=low

  * new upstream version (LP: #957924)
  * debian/control:
    + added Enhance field (Closes: #685291)
    + added python-poppler depends. (LP: #1015035)

 -- Andrea Gasparini <gaspa@yattaweb.it>  Sun, 02 Sep 2012 15:26:25 +0200

gedit-latex-plugin (3.4.0-1) unstable; urgency=low

  * new upstream version
  * debian/control: bump standard version to 3.9.3
  * debian/patches/: dropped patch 02 and 03 as already included in upstream

 -- Andrea Gasparini <gaspa@yattaweb.it>  Thu, 29 Mar 2012 23:54:37 +0200

gedit-latex-plugin (3.3.2-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS due to latex/latex/environment.py not in POTFILES.in":
    add patch 03-environment-translations.patch from Ubuntu / Mathieu
    Trudel-Lapierre: add latex/latex/environment.py to po/POTFILES.in.
    (Closes: #656669)

 -- gregor herrmann <gregoa@debian.org>  Sat, 24 Mar 2012 13:43:12 +0100

gedit-latex-plugin (3.3.2-1) unstable; urgency=low

  * new upstream version (Closes: #650355)
  * added gvfs-bin dependency

 -- Andrea Gasparini <gaspa@yattaweb.it>  Thu, 08 Dec 2011 13:27:14 +0100

gedit-latex-plugin (3.3.1-1) unstable; urgency=low

  * new upstream version (moving to gedit 3) (Closes: #635119)

 -- Andrea Gasparini <gaspa@yattaweb.it>  Tue, 04 Oct 2011 21:00:22 +0200

gedit-latex-plugin (0.2.0-1) unstable; urgency=low

  * new upstream version (Closes: #592974)
  * switching to source/format 3.0.
  * switching to dh7
  * debian/control: 
    + added Homepage field.
    + gedit with right case (LP: #567346)
  * debian/patches/series: dropping 05_fix_python26_specific_syntax.patch
  * debian/patches/07_debug_to_warning.patch: set log level to warning 
    (LP: #584462).  Thanks to Guillaume Melquiond 
  * debian/patches/06_master_document_not_loaded.patch: property not
    initialized before getting called. (Closes: #557035).
  * debian/changelog: specified GPL-2 or later for packaging.

 -- Andrea Gasparini <gaspa@yattaweb.it>  Fri, 24 Sep 2010 23:16:18 +0200

gedit-latex-plugin (0.2rc3-2) unstable; urgency=low

  * added README.source (mentions standard quilt readme.source) 
  * debian/patches: added patch that fix some python2.6-only sintax
    (Closes: #559135)
  * debian/control: added dependency to python-multiprocessing
    (Closes: #559176) and python-glade2

 -- Andrea Gasparini <gaspa@yattaweb.it>  Wed, 02 Dec 2009 10:51:14 +0100

gedit-latex-plugin (0.2rc3-1) unstable; urgency=low

  * new upstream release. (Closes: #524949 #557035)
  * debian/control: Depends on python-poppler (Closes: #552626)

 -- Andrea Gasparini <gaspa@yattaweb.it>  Mon, 23 Nov 2009 15:12:39 +0100

gedit-latex-plugin (0.2rc2-2) unstable; urgency=low

  * updated patches/01_images_in_usr_share.patch to handle both files and
    directories (Closes: 537459)

 -- Andrea Gasparini <gaspa@yattaweb.it>  Mon, 23 Nov 2009 15:12:19 +0100

gedit-latex-plugin (0.2rc2-1) unstable; urgency=low

  * new upstream release.
  * debian/{control,rules}: changed to quilt, bumped standard-version to
    3.8.2.

 -- Andrea Gasparini <gaspa@yattaweb.it>  Wed, 24 Jun 2009 21:46:21 +0200

gedit-latex-plugin (0.2rc1-2) unstable; urgency=low

  * debian/rules: fixed calls to pysupport (Closes: 514928)
  * debian/control:
    - added python-enchant as Recommends (Closes: 514859)
  * debian/pathes/03_changes_for_python26.patch: make plugin work with
    python2.6
  * debian/patches/04_name_clashing_with_bzr_gedit_plugin.patch: MenuToolAction
    clash with bzr plugin that has a gobject with the same name.

 -- Andrea Gasparini <gaspa@yattaweb.it>  Sat, 25 Mar 2009 18:10:46 +0100

gedit-latex-plugin (0.2rc1-1) unstable; urgency=low

  * New Upstream version
  * debian/control: added texlive as Recommends, set all as Architecture.
  * debian/patches: fixed a "reference before assignment" in latex/dialog.py

 -- Andrea Gasparini <gaspa@yattaweb.it>  Wed, 04 Feb 2009 15:59:34 +0100

gedit-latex-plugin (0.1.3.2-2) unstable; urgency=low

  * Fixed a wrong search path for some images. (Closes: 505141)
  * debian/control: reindented to remove lintian warning

 -- Andrea Gasparini <gaspa@yattaweb.it>  Sat, 15 Nov 2008 15:46:43 +0100

gedit-latex-plugin (0.1.3.2-1) unstable; urgency=low

  [Alessandro Scarozza]
    * Initial Release (Closes: #453391)

  [Andrea Gasparini]
    * debian/copyright: given the due to the authors. :)
    * debian/install: using dh_install file instead of install: target.
    * debian/control: added missing dependencies

 -- Andrea Gasparini <gaspa@yattaweb.it>  Thu, 04 Sep 2008 13:12:47 +0200
