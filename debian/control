Source: gedit-latex-plugin
Section: gnome
Priority: extra
Maintainer: Andrea Gasparini <gaspa@yattaweb.it>
Uploaders: Pietro Battiston <me@pietrobattiston.it>
Build-Depends: debhelper (>= 7.0.50~), libglib2.0-dev (>= 2.26.0), libgtk-3-dev (>= 3.0.12), intltool (>= 0.40), python (>= 2.6.6-3~)
Standards-Version: 3.9.4
Homepage: http://live.gnome.org/Gedit/LaTeXPlugin
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=collab-maint/gedit-latex.git
Vcs-Git: git://anonscm.debian.org/collab-maint/gedit-latex.git

Package: gedit-latex-plugin
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python:Depends}, gedit (>= 3.8), rubber, python-dbus,
    python (>=2.6.6-3~) | python-multiprocessing, python-glade2, gvfs-bin,
    python-poppler
Recommends: texlive, python-enchant
Enhances: gedit
Description: gedit plugin for composing and compiling LaTeX documents
 This plugin assist you in a number of task:
  - Code Completion: if you type a prefix it shows you all matching commands
    and the structure and meaning of their arguments. If possible it shows
    options for the argument the cursor is in.
  - Assistants: there are several assistants for frequent tasks like creating
    the body of a new LaTeX file, inserting a graphics, inserting a table or a
    matrix, inserting source code listings, inserting BibTeX entries.
  - BibTeX Integration: an outline view is created for BibTeX files and the
    LaTeX completion is aware of bibliographies included per \bibliography and
    proposes their entries at the \cite command. BibTeX entries may be inserted
    with the help of a dialog. 
  - Build System: The build system uses profiles like "PDF", "DVI" or
    "PostScript". Per default the plugin uses rubber for automated document
    compiling, but you may create your own profiles invoking the LaTeX command
    chain directly.

